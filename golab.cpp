#include "utils.h"

typedef pair<ll,int> pli;
const int inft = 1000000009;

#define DEBUG
#ifdef DEBUG
#define DEB printf
#else
#define DEB(...)
#endif

vi current, cpos;

ll compscore(int req) {
	int vid = requests[req].x.x;
	int endp = requests[req].x.y;
	int cnt = requests[req].y;
	if(cpos[req] == latencies[endp].size()) return -1;
	ll comp_score = current[req] - latencies[endp][cpos[req]].y;
	comp_score *= cnt;
	//comp_score *= 10000;
	//comp_score /= videoSizes[vid];
	return comp_score;
}

int main(){
	wczytaj();
	for(auto &lat : latencies) {
		sort(ALL(lat), [](pii a, pii b) { return a.y < b.y; });
	}

	vector<vi> v2reqs(videos);
	fru(i,n_requests) {
		auto req = requests[i];
		v2reqs[req.x.x].pb(i);
	}

	vector<vi> ce2lat(caches, vi(endpoints, inft));
	fru(i,endpoints) {
		for(pii p : latencies[i]) {
			if(p.x == -1) continue;
			ce2lat[p.x][i] = p.y;
		}
	}

	current.resize(n_requests);
	fru(i,n_requests) {
		int endp = requests[i].x.y;
		current[i] = latencies[endp].back().y;
	}

	fru(i,endpoints) {
		assert(latencies[i].back().x == -1);
		latencies[i].pop_back();
	}

	cpos.resize(n_requests, 0);
	set<pli> chg; // (score, request)
	vi cachecap(caches, capacity);
	vector<vector<bool>> ans(caches,vector<bool>(videos));

	fru(req,n_requests) {
		ll comp_score = compscore(req);
		if(comp_score <= 0) continue;
		chg.insert(pli(comp_score, req));
	}

	while(1) {
		pli ch = pli(-1,-1);
		while(ch == pli(-1,-1)) {
			if(chg.empty()) break;
			pli ch2 = *chg.rbegin();
			chg.erase(ch2);

			int req = ch2.y;
			int vid = requests[req].x.x;
			int endp = requests[req].x.y;
			int cach = latencies[endp][cpos[req]].x;

			if(cachecap[cach] < videoSizes[vid]) {
				cpos[req]++;
				ll comp_score = compscore(req);
				if(comp_score <= 0) continue;
				chg.insert(pli(comp_score, req));
				continue;
			}

			ll comp_score = compscore(req);
			if(comp_score != ch2.x) {
				assert(comp_score < ch2.x);
				if(comp_score <= 0) continue;
				chg.insert(pli(comp_score, req));
				continue;
			}

			ch = ch2;
			break;
		}

		if(ch == pli(-1,-1)) break;
		int req = ch.y;
		int vid = requests[req].x.x;
		int endp = requests[req].x.y;
		int cach = latencies[endp][cpos[req]].x;
		cachecap[cach] -= videoSizes[vid];
		ans[cach][vid] = 1;
		for(int req2 : v2reqs[vid]) {
			assert(requests[req2].x.x == vid);
			int endp2 = requests[req2].x.y;
			if(ce2lat[cach][endp2] < current[req2]) {
				current[req2] = ce2lat[cach][endp2];
			}
		}
	}

	wypisz(ans);

	return 0;
}

