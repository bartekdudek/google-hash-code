#!/bin/bash

#usage: ./odpal.sh <name of binary>

for i in in/*.in; do 
	test=${i%.in}
	echo $test":"
	./$1 < $test.in > $test.out
	./score $test.in $test.out
done

