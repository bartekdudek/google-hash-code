#include <bits/stdc++.h>
using namespace std;

#define fru(j,n) for(int j=0; j<(n); ++j)
#define tr(it,v) for(auto it=(v).begin(); it!=(v).end(); ++it)
#define x first
#define y second
#define pb push_back
#define ALL(G) (G).begin(),(G).end()

typedef double D;
typedef long long ll;
typedef long long LL;
typedef pair<int,int> pii;
typedef vector<int> vi;

const int mod = 1000000007;
const int MAXN = 1000006;

int videos, endpoints, n_requests, caches, capacity;
vi videoSizes;
vector<vector<pii>> latencies; // endpoint -> (cache, latency)
vector<pair<pii, int>> requests; // ((video, endpoint), count)

void wczytaj() {
	scanf("%d%d%d%d%d", &videos, &endpoints, &n_requests, &caches, &capacity);

	videoSizes.resize(videos);
	fru(i,videos) scanf("%d",&videoSizes[i]);

	latencies.resize(endpoints);
	fru(i,endpoints) {
		int L,K;
		scanf("%d%d",&L,&K);
		latencies[i].resize(K+1);
		latencies[i][0] = pii(-1,L);
		fru(j,latencies[i].size()-1) {
			scanf("%d%d", &latencies[i][j+1].x, &latencies[i][j+1].y);
		}
	}

	requests.resize(n_requests);
	fru(i,n_requests) {
		int v,e,n;
		scanf("%d%d%d",&v,&e,&n);
		requests[i] = make_pair(pii(v,e),n);
	}
}

ll get_score(const vector<vector<bool> > &T) { // -1 -> invalid
	//correctness
	for(auto cache:T){
		ll used=0;
		fru(i,cache.size()) if(cache[i]) used+=videoSizes[i];
		if(used>capacity) return -1;
	}
	//score
	ll ret=0;
	for(auto req: requests){
		int _video=req.x.x,_end=req.x.y;
		int min_lat=latencies[_end][0].y;
		//compute min
		for(auto it: latencies[_end]){
			if(it.x!=-1 && T[it.x][_video])min_lat=min(min_lat,it.y);
		}
		ret+=1LL*req.y*(latencies[_end][0].y-min_lat);
	}
	ll opt=0;
	for(auto a:requests){
		opt+=a.y;
	}
	return ret*1000/opt;
}

void wypisz(const vector<vector<bool>> &T) {
	printf("%lu\n",T.size());
	fru(i,T.size()) {
		printf("%d ",i);
		fru(j,T[i].size()) if(T[i][j]) printf("%d ",j);
		puts("");
	}
}
