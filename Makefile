CXXFLAGS := -O2 -Wall -Wextra -Wshadow -Wno-sign-compare -U_FORTIFY_SOURCE
CXXFLAGS += -std=gnu++11
# STL debug
#CXXFLAGS += -g -O0 -D_GLIBCXX_DEBUG

%: %.cpp utils.h
	$(CXX) $(CXXFLAGS) $< -o $@
