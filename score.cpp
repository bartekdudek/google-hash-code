#include "utils.h"

#define DEBUG
#ifdef DEBUG
#define DEB printf
#else
#define DEB(...)
#endif

int main(int argc, char *argv[]) {
	if(argc != 3) {
		fprintf(stderr, "usage: %s <in> <out>", argv[0]);
		return 1;
	}

	if(freopen(argv[1], "r", stdin) == NULL) return 2;

	wczytaj();

	if(freopen(argv[2], "r", stdin) == NULL) return 2;

	int lines;
	scanf("%d",&lines);
	assert(lines <= caches);

	vector<vector<bool>> res(caches);
	fru(i,lines) {
		int C;
		scanf("%d", &C);
		assert(0 <= C && C < caches);
		assert(res[C].empty());
		res[C].resize(videos);

		while(1) {
			char c; int v;
			do {
				c=getchar();
			} while(c==' ');
			if(c=='\n') break;
			ungetc(c, stdin);
			scanf("%d",&v);
			assert(res[C][v] == false);
			res[C][v] = true;
		}
	}

	fru(i,caches) if(res[i].empty()) res[i].resize(videos);

	printf("%lld\n", get_score(res));
	return 0;
}

