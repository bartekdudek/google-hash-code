#include "utils.h"

#define DEBUG
#ifdef DEBUG
#define DEB printf
#else
#define DEB(...)
#endif

typedef pair<ll,int> pli;
const int inft = 1000000009;

vector<vector<bool> > X;

namespace gg {

vi current, cpos;
vector<vector<pii>> latencies;

ll compscore(int req) {
	int vid = requests[req].x.x;
	int endp = requests[req].x.y;
	int cnt = requests[req].y;
	if(cpos[req] == latencies[endp].size()) return -1;
	ll comp_score = current[req] - latencies[endp][cpos[req]].y;
	comp_score *= cnt;
	//comp_score *= 10000;
	//comp_score /= videoSizes[vid];
	return comp_score;
}

int main(){
	wczytaj();
	latencies = ::latencies;

	for(auto &lat : latencies) {
		sort(ALL(lat), [](pii a, pii b) { return a.y < b.y; });
	}

	vector<vi> v2reqs(videos);
	fru(i,n_requests) {
		auto req = requests[i];
		v2reqs[req.x.x].pb(i);
	}

	vector<vi> ce2lat(caches, vi(endpoints, inft));
	fru(i,endpoints) {
		for(pii p : latencies[i]) {
			if(p.x == -1) continue;
			ce2lat[p.x][i] = p.y;
		}
	}

	current.resize(n_requests);
	fru(i,n_requests) {
		int endp = requests[i].x.y;
		current[i] = latencies[endp].back().y;
	}

	fru(i,endpoints) {
		assert(latencies[i].back().x == -1);
		latencies[i].pop_back();
	}

	cpos.resize(n_requests, 0);
	set<pli> chg; // (score, request)
	vi cachecap(caches, capacity);
	vector<vector<bool>> ans(caches,vector<bool>(videos));

	fru(req,n_requests) {
		ll comp_score = compscore(req);
		if(comp_score <= 0) continue;
		chg.insert(pli(comp_score, req));
	}

	while(1) {
		pli ch = pli(-1,-1);
		while(ch == pli(-1,-1)) {
			if(chg.empty()) break;
			pli ch2 = *chg.rbegin();
			chg.erase(ch2);

			int req = ch2.y;
			int vid = requests[req].x.x;
			int endp = requests[req].x.y;
			int cach = latencies[endp][cpos[req]].x;

			if(cachecap[cach] < videoSizes[vid]) {
				cpos[req]++;
				ll comp_score = compscore(req);
				if(comp_score <= 0) continue;
				chg.insert(pli(comp_score, req));
				continue;
			}

			ll comp_score = compscore(req);
			if(comp_score != ch2.x) {
				assert(comp_score < ch2.x);
				if(comp_score <= 0) continue;
				chg.insert(pli(comp_score, req));
				continue;
			}

			ch = ch2;
			break;
		}

		if(ch == pli(-1,-1)) break;
		int req = ch.y;
		int vid = requests[req].x.x;
		int endp = requests[req].x.y;
		int cach = latencies[endp][cpos[req]].x;
		cachecap[cach] -= videoSizes[vid];
		ans[cach][vid] = 1;
		for(int req2 : v2reqs[vid]) {
			assert(requests[req2].x.x == vid);
			int endp2 = requests[req2].x.y;
			if(ce2lat[cach][endp2] < current[req2]) {
				current[req2] = ce2lat[cach][endp2];
			}
		}
	}

	X = ans;
	fprintf(stderr, "golab: %lld\n",get_score(X));

	return 0;
}


}

vector<vector<bool> > bestX;
vector<multiset<int> >best_lat; 
vector<int>used;//endpoints -latencies
ll best_score,curr_score;
map<int,vi> M;//video ->requests
vector<vector<int> > L; // cache,end
bool tryinsert(){
//	printf("score %d\nused\n",curr_score);
//	fru(i,caches)printf("%d " ,used[i]);puts("");
//	printf("lat\n");
//	fru(i,n_requests){
//		for(auto a:best_lat[i])printf("%d ",a);puts("");
//	}
	ll ret=curr_score;
	pii lead(-1,-1);
	int cnt=0;
	while(cnt++<5000){
		int i=rand()%caches,j=rand()%videos;
		if(!X[i][j] && used[i]+videoSizes[j]<=capacity){
		ll temp=curr_score;
		//update score
		for(auto a:M[j]){
		//	printf("%d %d req %d, l %d, curr %lld\n",i,j,a,L[i][requests[a].x.y],curr_score);
			temp+=1LL*requests[a].y*(*best_lat[a].begin()-min(*best_lat[a].begin(),L[i][requests[a].x.y]));
		}
		if(temp>ret){ret=temp;lead=pii(i,j);}
	}
	}
	curr_score=ret;
	if(lead==pii(-1,-1))return 0;
	X[lead.x][lead.y]=1;
	if(curr_score>best_score){
	//	printf("update\n");
		bestX=X;best_score=curr_score;}
//	printf("%d %d\n",lead.x,lead.y);
	//insert to bestX
	//update best_lat
	for(auto a: M[lead.y])best_lat[a].insert(L[lead.x][requests[a].x.y]);
	used[lead.x]+=videoSizes[lead.y];
	return 1;
}
void tryerase(){	
//	printf("score %d\nused\n",curr_score);
//	fru(i,caches)printf("%d " ,used[i]);puts("");
	ll bes=0;
	pii lead(-1,-1);
	int cnt=0;
	while(cnt++<10000 || lead==pii(-1,-1)){
		int i=rand()%caches,j=rand()%videos;
		
		if(X[i][j]){
		ll temp=curr_score;
		//update score
		for(auto a:M[j]){
			int uu=*best_lat[a].begin();
			best_lat[a].erase(best_lat[a].find(L[i][requests[a].x.y]));
			temp+=1LL*requests[a].y*(uu-*best_lat[a].begin());
			best_lat[a].insert(L[i][requests[a].x.y]);
		}
		if(temp>bes){bes=temp;lead=pii(i,j);}
	}
	}
//	printf("erase %d %d\n",lead.x,lead.y);
	X[lead.x][lead.y]=0;
	for(auto a:M[lead.y])best_lat[a].erase(best_lat[a].find(L[lead.x][requests[a].x.y]));
	used[lead.x]-=videoSizes[lead.y];
	//update curr score
	curr_score=bes;
}
int main() {
	gg::main();
	//
	srand(42);
	//create L
	L.resize(caches,vector<int>(endpoints,mod));
	fru(i,endpoints)for(auto b:latencies[i])if(b.x!=-1)L[b.x][i]=min(L[b.x][i],b.y);
	//create M
	fru(i,n_requests)M[requests[i].x.x].push_back(i);
	best_lat.resize(n_requests);
	//put latency to server
	fru(i,n_requests)best_lat[i].insert(latencies[requests[i].x.y][0].y);
	used.resize(caches,0);
	fru(i,caches)fru(j,videos)if(X[i][j])used[i]+=videoSizes[j];
	fru(i,caches)fru(j,videos)if(X[i][j]){
		fru(h,n_requests)if(requests[h].x.x==j)
			best_lat[h].insert(L[i][requests[h].x.y]);
	}
	if(0) fru(h,n_requests){
		for(auto a: best_lat[h])printf("%d ",a);puts("");
	}

	
	curr_score=0;
	fru(i,n_requests)curr_score+=1LL*requests[i].y*
		(latencies[requests[i].x.y][0].y-*best_lat[i].begin());
	ll prev=curr_score;
	int tr=0; 	
	while(++tr){
		if(!tryinsert())tryerase();
		if(tr%100==0){
			if(best_score==prev)break;
			prev=best_score;
			fprintf(stderr,"it %d::%lld\n",tr,get_score(bestX));
		}
	}
	wypisz(bestX);
//	printf("%lld\n",get_score(bestX));	
	return 0;
}

